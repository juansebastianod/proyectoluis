package parqueadero;

import java.util.*;

/**
 *
 * @author Matias
 */
public class Test {

    public static void main(String[] args) {
        Garaje g = new Garaje();
        Scanner sc = new Scanner(System.in);
        System.out.println("cantidad de casos");
        int a = sc.nextInt();
        for (int i = 0; i < a; i++) {
            System.out.println("digite matricula");
            String matricula = sc.next();
            System.out.println("digite direccion del dueño");
            String direccion = sc.next() + " " + sc.nextLine();
            Coche nueva = g.busqueda(direccion, matricula);
            Coche dos = new Coche(direccion, matricula);
            if (nueva != null) {
                System.out.println("digite descripcion");
                String descripcion = sc.next()+ " " +sc.nextLine();
                System.out.println("digite kilometros");
                int kms = sc.nextInt();
                nueva.addReparacion(descripcion, kms);
                
            } else {
                System.out.println("digite descripcion");
                String descripcion = sc.next()+ " " +sc.nextLine();
                System.out.println("digite kilometros");
                int kms = sc.nextInt();
                dos.addReparacion(descripcion, kms);
                g.addCoche(dos);
            }
        }
        System.out.println(g);
    }
}
